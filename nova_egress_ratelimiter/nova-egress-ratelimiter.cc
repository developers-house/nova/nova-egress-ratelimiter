#include "nova-egress-ratelimiter.hpp"

namespace Envoy::Http {
    NovaRatelimiterConfig::NovaRatelimiterConfig(
        const novaegressratelimiting::Decoder& protoConfig
    ) : host_(protoConfig.host()), port_(protoConfig.port()) {}

    NovaRatelimiterFilter::NovaRatelimiterFilter(
        NovaRatelimiterConfigSharedPtr config
    ) : config_(config) {}

    NovaRatelimiterFilter::~NovaRatelimiterFilter() {}

    void NovaRatelimiterFilter::onDestroy() {}

    // we set outgoing headers to the discord servers, aka, we inject the token into it.
    FilterHeadersStatus NovaRatelimiterFilter::decodeHeaders(RequestHeaderMap& headers, bool) {
        headers.remove(LowerCaseString("x-forwarded-proto"));
        headers.setCopy(LowerCaseString("host"), "discord.com");
        headers.setCopy(LowerCaseString("Authorization"), this->config_->getDiscordToken());

        return FilterHeadersStatus::Continue;
    }

    FilterHeadersStatus NovaRatelimiterFilter::encodeHeaders(ResponseHeaderMap& headers, bool) {
        headers.addCopy(LowerCaseString("x-nova-test-header"), "yo");
        return FilterHeadersStatus::Continue;
    }

    FilterDataStatus NovaRatelimiterFilter::decodeData(Buffer::Instance&, bool) {
        return FilterDataStatus::Continue;
    }

    void NovaRatelimiterFilter::setDecoderFilterCallbacks(StreamDecoderFilterCallbacks& callbacks) {
        decoder_callbacks_ = &callbacks;
    }

    void NovaRatelimiterFilter::setEncoderFilterCallbacks(StreamEncoderFilterCallbacks& callbacks) {
        encoder_callbacks_ = &callbacks;
    }
}