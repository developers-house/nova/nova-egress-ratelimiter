#include <string>
#include "envoy/registry/registry.h"
#include "envoy/server/filter_config.h"

#include "nova-egress-ratelimiter.hpp"

#include "nova-egress-ratelimiter/nova_ratelimiting.pb.h"
#include "nova-egress-ratelimiter/nova_ratelimiting.pb.validate.h"

namespace Envoy::Server::Configuration
{
    // Utility for building a nova-ratelimiter from a protobuf config.
    // this builds the nova rate-limiter filter instance from the protocol buffer
    // config provided by the user in the envoy configuration file.
    class NovaEgressRatelimiterConfigFactory : public NamedHttpFilterConfigFactory {
        public:

        // creates the configuration callback using createFilter using the provided protocol
        Http::FilterFactoryCb createFilterFactoryFromProto(
            const Protobuf::Message& protoConfig,
            const std::string&,
            FactoryContext& context
        ) override {
            return createFilter(
                Envoy::MessageUtil::downcastAndValidate<const novaegressratelimiting::Decoder&>(protoConfig, context.messageValidationVisitor()),
                context
            );
        }

        // returns a empty instance of the nova configuration protocol buffer.
        ProtobufTypes::MessagePtr createEmptyConfigProto() override {
            return ProtobufTypes::MessagePtr { new novaegressratelimiting::Decoder() };
        }

        // returns the name of the envoy filter
        std::string name() const override { return "nova-egress-ratelimiter"; }

        private:

        // builds the configuration & returns the filter chain callback that
        // registers the stream decoder.
        Http::FilterFactoryCb createFilter (
            const novaegressratelimiting::Decoder& protoConfig,
            FactoryContext&
        ) {
            Http::NovaRatelimiterConfigSharedPtr config = 
                std::make_shared<Http::NovaRatelimiterConfig>(
                    Http::NovaRatelimiterConfig(protoConfig)
                );
            return [config](Http::FilterChainFactoryCallbacks& callbacks) -> void {
                auto decoderFilter = new Http::NovaRatelimiterFilter(config);
                auto encoderFilter = new Http::NovaRatelimiterFilter(config);
                callbacks.addStreamEncoderFilter(Http::StreamEncoderFilterSharedPtr{ encoderFilter });
                callbacks.addStreamDecoderFilter(Http::StreamDecoderFilterSharedPtr{ decoderFilter });
            };
        }
    };
    // we register our filter to the envoy static register.
    static Registry::RegisterFactory<NovaEgressRatelimiterConfigFactory, NamedHttpFilterConfigFactory> register_;
} // namespace Envoy
