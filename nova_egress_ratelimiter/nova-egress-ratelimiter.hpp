#include "envoy/registry/registry.h"
#include "nova-egress-ratelimiter/nova_ratelimiting.pb.h"
#include "nova-egress-ratelimiter/nova_ratelimiting_server.pb.h"
#include "source/extensions/filters/http/common/pass_through_filter.h"

namespace Envoy::Http {
    class NovaRatelimiterConfig {
        public:
        NovaRatelimiterConfig(const novaegressratelimiting::Decoder& protoConfig);
        const std::string host () const { return host_; }
        int port () const { return port_; };
        std::string getDiscordToken () const { return authorizationMode_ + std::getenv("DISCORD_TOKEN"); }

        private:
        const std::string authorizationMode_ = "Bot ";
        const std::string host_;
        const int port_;

    };

    using NovaRatelimiterConfigSharedPtr = std::shared_ptr<NovaRatelimiterConfig>;
    class NovaRatelimiterFilter : public StreamFilter, public PassThroughDecoderFilter, public PassThroughEncoderFilter {
        public:
        NovaRatelimiterFilter(NovaRatelimiterConfigSharedPtr);
        ~NovaRatelimiterFilter();
        // Http::StreamFilterBase
        void onDestroy() override;
        // http::StreamDecoderFilter
        FilterHeadersStatus decodeHeaders(RequestHeaderMap&, bool) override;
        FilterDataStatus decodeData(Buffer::Instance&, bool) override;

        FilterHeadersStatus encodeHeaders(ResponseHeaderMap&, bool) override;

        void setDecoderFilterCallbacks(StreamDecoderFilterCallbacks&) override;
        void setEncoderFilterCallbacks(StreamEncoderFilterCallbacks&) override;
        private:
        const NovaRatelimiterConfigSharedPtr config_;
        StreamDecoderFilterCallbacks* decoder_callbacks_;
        StreamEncoderFilterCallbacks* encoder_callbacks_;
    };
}