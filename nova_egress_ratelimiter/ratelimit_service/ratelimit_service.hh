#pragma once

#include <chrono>
#include <cstdint>
#include <string>
#include <vector>

#include "envoy/grpc/async_client.h"
#include "envoy/grpc/async_client_manager.h"

#include "source/common/grpc/typed_async_client.h"
#include "source/common/singleton/const_singleton.h"
#include "nova_egress_ratelimiter/ratelimit_service/ratelimit_service.pb.h"
#include "common/common/logger.h"

// This class implements the gRPC client for the Nova's Ratelimit service.
namespace nova::ratelimit {

    using RatelimitAsyncCallbacks = Envoy::Grpc::AsyncRequestCallbacks<v1::RatelimitResponse>;
    using RatelimitUpdateAsyncCallbacks = Envoy::Grpc::AsyncRequestCallbacks<v1::CreateBucketData>;

    class RatelimitService :    public RatelimitAsyncCallbacks,
                                public Envoy::Logger::Loggable<Envoy::Logger::Id::filter>
    {
    private:
        
    public:
        RatelimitService(
            Envoy::Grpc::RawAsyncClientPtr&& asyncClient,
            const absl::optional<std::chrono::milliseconds>& timeout
        );
        ~RatelimitService ();

        void getRatelimit ();
        void cancel ();

        // This is a void method because we do not need to add additional headers
        // to the ratelimit connexion.
        void onCreateInitialMetadata (Envoy::Http::RequestHeaderMap&) override {}

        // Callback called when a ratelimit response is received.
        void onSuccess (std::unique_ptr<v1::RatelimitResponse>&&, Envoy::Tracing::Span&) override;
    
    };
    /**
     * This service is used to send data to the ratelimiting service when a bucket is not known by the service.
     */
    class RatelimitUpdateService :  public RatelimitUpdateAsyncCallbacks,
                                    public Envoy::Logger::Loggable<Envoy::Logger::Id::filter>
    {
    private:

    public:
        RatelimitUpdateService (
            Envoy::Grpc::RawAsyncClientPtr&& asyncclient,
            const absl::optional<std::chrono::milliseconds>& timeout
        );
        ~RatelimitUpdateService ();

        void createBucket (v1::);
        void cancel ();

        void onCreateInitialMetadata (Envoy::Http::RequestHeaderMap&) override {}
        void onSuccess (std::unique_ptr<v1::CreateBucketData>&&, Envoy::Tracing::Span&) override;
    };
}